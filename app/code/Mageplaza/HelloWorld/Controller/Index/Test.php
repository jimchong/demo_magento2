<?php
namespace Mageplaza\HelloWorld\Controller\Index;

class Test extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;

	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory)
	{
		$this->_pageFactory = $pageFactory;
		return parent::__construct($context);
	}

	public function execute()
	{
//        var_dump('abc');
//        var_dump(__METHOD__);
//        echo '<p>You Did It!</p>';

        $page_object = $this->_pageFactory->create();;
        return $page_object;

//		echo "Hello World";
//		exit;
	}
}

