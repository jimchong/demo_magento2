<?php
namespace Pulsestorm\ToDoCrud\Model\ResourceModel;
class TodoItem extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    const CACHE_TAG = 'pulsestorm_todocrud_todoitem';


    protected function _construct()
    {
        $this->_init('pulsestorm_todocrud_todoitem', 'pulsestorm_todocrud_todoitem_id');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
